from flask import Flask, render_template, request
import time
import json

last_records = [{"timestamp": "12:00", "temp": 12 },
               {"timestamp": "12:00", "temp": 12 },
               {"timestamp": "12:00", "temp": 12 },
               {"timestamp": "12:00", "temp": 12 },
               {"timestamp": "12:00", "temp": 12 },
               {"timestamp": "12:00", "temp": 12 },
               {"timestamp": "12:00", "temp": 12 }]

def addUser(username, password):
    data = parseJson();
    new_index = int(getLastIndex(data)) + 1
    new_index = str(new_index)
    data[new_index] = {'user':username, 'password': password}
    with open('tempMonitoringProject/project/users.json', 'w') as f:
        json.dump(data, f)


def getLastIndex(data):
    last_index = 0
    for i in data:
        last_index = i
    return last_index

def parseJson():
    with open('tempMonitoringProject/project/users.json', 'r') as f:
        data = json.load(f)
    return data


app = Flask(__name__)  

@app.route('/login', methods=['GET','POST'])
def login_form():
    return render_template("login.html")
    # if request:
    #     if request.form:
    #         username = request.form['username']
    #         password = request.form['password']
    #         if not username or not password:
    #             error = 'Both username and password are required.'
    #             return render_template('login.html', error=error)
    
    #         elif username != 'admin' or password != 'admin':
    #             data = parseJson();
    #             if data:
    #                 for i in data:
    #                     if data[i]['user'] == username and data[i]['password'] == password:
    #                         return render_template('dashboard.html', last_records = last_records, last_record = {'timestamp':'12:00', 'temp': '12'})
    #             error = 'Invalid username or password.'
    #             return render_template('login.html', error=error)
    # return render_template('login.html')


@app.route('/register', methods=['GET','POST'])
def register():
    if request:
        if request.form:
            username = request.form['username']
            password = request.form['password']
            conf_password = request.form['password-confirm']

            if(conf_password != password):
                error = "Passwords doesn't match"
                return render_template('register.html', error = error)
            data = parseJson()
            if(data):
                for i in data:
                    if data[i]['user'] == username:
                        error = "This user already exist"
                        return render_template('register.html', error=error)    
                addUser(username, password)
                return render_template('login.html', message = "new account created")
    return render_template('register.html')

@app.route('/dashboard', methods=['GET','POST'])
def dashboard():
    if request:
        if request.form:
            username = request.form['username']
            password = request.form['password']
            if not username or not password:
                error = 'Both username and password are required.'
                return render_template('login.html', error=error)
    
            elif username != 'admin' or password != 'admin':
                data = parseJson();
                if data:
                    for i in data:
                        if data[i]['user'] == username and data[i]['password'] == password:
                            return render_template('dashboard.html', last_records = last_records, last_record = {'timestamp':'12:00', 'temp': '12'})
                error = 'Invalid username or password.'
                return render_template('login.html', error=error)
    return render_template('login.html')
    #return render_template('dashboard.html', last_records = last_records, last_record = {'timestamp':'12:00', 'temp': '12'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8002', debug=True, use_reloader=False)



